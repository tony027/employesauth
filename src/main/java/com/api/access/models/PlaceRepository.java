package com.api.access.models;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlaceRepository extends MongoRepository<PlacesModel, String> {

    @Query("{'name':?0}")
    public PlacesModel findByName(String name);

}
