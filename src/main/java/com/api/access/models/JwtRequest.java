package com.api.access.models;

import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document("granting")
public class JwtRequest  implements Serializable {

    private String id;
    private String app;
    private String password;

    public JwtRequest() {
    }

    public JwtRequest(String id, String app, String password) {
        this.setId(id);
        this.setApp(app);
        this.setPassword(password);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
