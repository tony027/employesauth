package com.api.access.models;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends MongoRepository<UserModel, String> {

    @Query("{'userCode':?0}")
    public UserModel findByUser(String userCode);

    @Query("{'userCode':?0, 'password':?0}")
    public List<UserModel> findByUserPassword(String userCode, String password);

}
