package com.api.access.config.controllers;

import com.api.access.models.PlacesModel;
import com.api.access.service.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class PlacesV1Controller {

    @Autowired
    PlaceService placeService;

    @GetMapping(value = "/v1/places", produces = "application/json")
    public ResponseEntity<List<PlacesModel>> getPlaces() {
        List<PlacesModel> list = placeService.findAll();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping(value = "/v1/places/{name}", produces = "application/json")
    public ResponseEntity<List<PlacesModel>> getPlace(@PathVariable String name) {
        List<PlacesModel> list = placeService.findByName(name);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    /* Add nuevo producto */
    @PostMapping(value = "/v1/places", produces="application/json")
    public ResponseEntity<String> addProducto(@RequestBody PlacesModel placeNew)
    {
        System.out.println(placeNew.toString());
        placeService.save(placeNew);
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    // Update not idempotent
    @PatchMapping(value = "/v1/places/{name}")
    public ResponseEntity updatePrecioProducto(@RequestBody PlacesModel placeUpdate,
                                               @PathVariable String name){
        Optional<placeUpdate> pr = placeService.findByName(name);
        if (!pr.isPresent()) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        pr.get().precio = productoPrecio.getPrecio();
        System.out.println(pr.get());
        productoService.save(pr.get());
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }


}
