package com.api.access.config.controllers;

import com.api.access.models.PlaceRepository;
import com.api.access.models.UserModel;
import com.api.access.models.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * @author: Equipo 1
 * @version: 29/04/21
 */

@RestController
public class ControllerUserV1 {
    @Autowired
    private UserRepository repository;
    /**
     * Method getUsers
     * Listara todos los usuarios encontrados x findall
     * No requiere parametro de entrada
     */
    @GetMapping(value = "/v1/users/all", produces = "application/json")
    public ResponseEntity<List<UserModel>> getUsers(){
        System.out.println("Method ObtenerUsuarios");
        List<UserModel> listaUser = repository.findAll();
        return new ResponseEntity<List<UserModel>>(listaUser, HttpStatus.OK);
    }
    /**
     * Method getUserbyId
     * Busqueda de usuario x id
     * @param "idUser" corresponde a codigo de usuario para buscar en DB
     * @return ResponseEntity con los datos del user
     */

    @GetMapping(value = "/v1/users/{idUser}", produces = "application/json")
    public ResponseEntity<UserModel> getUserbyId(@PathVariable String idUser ){
        System.out.println("Method Obtener Usuario by ID");
        UserModel result = null;
        ResponseEntity<UserModel> responseFinal=null;
        try {
            result = (UserModel) repository.findByUser(idUser);
            responseFinal = new ResponseEntity<UserModel>(result, HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            responseFinal = new ResponseEntity("Usuario No Encontrado", HttpStatus.NOT_FOUND);
        }
        return responseFinal;
    }

    /**
     * Method postUserID
     * Crear un nuevo usuario
     * @param "UserModel": corresponde al usuario creado de tipo UserModel
     * @return ResponseEntity con el response
     */

    @PostMapping(value = "/v1/users")
    public ResponseEntity<String> addUsers(@RequestBody UserModel userNew)
    {
        try {
            UserModel resulInsert = repository.insert(userNew);
            return new ResponseEntity<String>(resulInsert.toString(), HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<String>("No es posible crear usuario", HttpStatus.NOT_ACCEPTABLE);
        }
    }

    /**
     * Method updateUserData
     * modificar parametros de un usuario
     * @param "UserModel": corresponde al usuario creado de tipo UserModel
     * @return ResponseEntity con el response
     */

    @PutMapping(value="/v1/users/{idUser}")
    public ResponseEntity<String> updateUserData(@PathVariable String userCode, @RequestBody UserModel userModelUp)
    {
        Optional <UserModel> usuario = repository.findById(repository.findByUser(userCode).getId());
        try {
            if(usuario.isPresent()){
                UserModel modifyUser = usuario.get();
                modifyUser.setName(userModelUp.name);
                modifyUser.setLastName(userModelUp.lastName);
                modifyUser.setRol(userModelUp.rol);
                modifyUser.setUserCode(userModelUp.userCode);
                UserModel userUpdate = repository.save(usuario.get());
                return new ResponseEntity<String>((usuario.toString()+"Modificado"), HttpStatus.OK);
            }
            else{
                return new ResponseEntity<String>("Usuario No Encotrado", HttpStatus.NOT_FOUND);
            }
        }
        catch (Exception e){
            return new ResponseEntity<String>("ERROR", HttpStatus.SERVICE_UNAVAILABLE);
        }
    }


}
