package com.api.access.service;

import com.api.access.models.PlaceRepository;
import com.api.access.models.PlacesModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PlaceService {

    @Autowired
    PlaceRepository placeRepository;

    // READ Documents
    public List<PlacesModel> findAll() {
        return placeRepository.findAll();
    }

    // READ One Document
    public Optional<PlacesModel> findById(String id) {
        return placeRepository.findById(id);
    }

    // READ One Document
    public Optional<PlacesModel> findByName(String name) {
        return placeRepository.findByName(name);
    }

    // CREATE Document
    public PlacesModel save(PlacesModel placesModel) {
        return placeRepository.save(placesModel);
    }

}
