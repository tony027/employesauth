package com.api.access.service;

import com.api.access.models.JwtRequest;
import com.api.access.models.TokenRepository;
import com.api.access.models.UserModel;
import com.api.access.models.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    TokenRepository tokenRepository;

    private final String password = "$2y$12$DxmQvHrtKKUB/lsy9OU8QOuuc0KwiU22js3E8hxbBH0GqSgE6nvne";

    @Override
    public UserDetails loadUserByUsername(String user) throws UsernameNotFoundException {
        if(validateApp(user) != null) {
            return new User(user, password, new ArrayList<>());
        }
        else {
            throw new UsernameNotFoundException(String.format("Usuario %s no encontrado", user));
        }
    }

    /* Validación DB UserModel */
    public JwtRequest validateApp (String userCode) {
        JwtRequest grantingModel = null;
        grantingModel = tokenRepository.findByApp(userCode);

        return grantingModel;
    }

}
