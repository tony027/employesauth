------------------------------------
------------------------------------
--------	ACCESS PLACES	--------
------------------------------------
------------------------------------

Equipo1PractitionerBack

	/access/authenticate/
1. 	Request []: authenticate POST (User, Password, Rol)
      Response []: Token

	/access/users/
	/access/users/{user-id}
	/access/users/{user-id}/places/
	/access/users/{user-id}/places/{place-id}/
2.	Request []: header: token
      getUser, createUser, updateUser, deleteUser GET,POST,PUT,PATCH,DELETE (User, Rol)
      Response []: User

	/access/users/{user-id}/qr-code/
4. 	Request []: header: token
      createQR POST (User)
      Response []: Image QR

	/access/places/{place-id}/
3.  Request []: header: token
    getPlaces, createPlaces, updatePlaces GET,POST,PATCH (Token, Rol)
    Response []: Places


Model:

{
"id": 23123,
"user": {
"identification": 112233,
"name": "Test",
"lastName": "Test",
"password": 123132123,
"rol": ""
"workday": {
"starTime": "",
"endTime": ""
}
},
"places": [
{
"id":"",
"description":"Piso 1",
"available": true,
},
{
"id":"",
"description":"",
"available": true
}
]
}